<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Inscription</title>
	<link rel="stylesheet" href="style/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
</head>
<body>


<div class="container-md">

        <div class="container-sm">
            <img src="img/logo.jpg" class="container-sm" alt="Image du logo Eat & Love">
        </div>
    
          <nav class="navbar navbar-expand-lg bg-success">
            <div class="container-fluid">
              <a class="navbar-brand text-danger" href="Accueil">Eat & Love</a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                  <li class="nav-item">
                    <a class="nav-link text-white" href="Carte">Notre Carte</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="Authentification">Login</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="Inscription" >Inscription</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="Menu">Mon Menu</a>
                  </li>
              </div>
            </div>
          </nav>
          
          <p class="alert alert-danger" role="alert">
<% 



if (request.getAttribute("login")==null){
	out.print("veuillez rentrer un login valide");
	
}else{
	out.print("inscription valider ");
}
%>
</p>
          
<form action=Inscription method="post">
	<div class="mb-3">
   		<label for="InputIdentifiant" class="form-label">Identifiant (votre futur login)</label>
    	<input type="text" class="form-control" name="InputIdentifiant">
 	 </div>

	<div class="mb-3">
    <label for=InputNom" class="form-label">Nom</label>
    <input type="text" class="form-control" name="InputNom">
  </div>
  
  <div class="mb-3">
    <label for="InputPrenom" class="form-label">Pr�nom</label>
    <input type="text" class="form-control" name="InputPrenom">
  </div>

  <div class="mb-3">
    <label for="InputEmail" class="form-label">Email address</label>
    <input type="email" class="form-control" name="InputEmail">
  </div>
  
  <div class="mb-3">
    <label for="InputTel" class="form-label">N� Tel</label>
    <input type="text" class="form-control" name="InputTel">
  </div>
  
  <div class="mb-3">
    <label for="InputAdress" class="form-label">Adresse</label>
    <input type="text" class="form-control" name="InputAdress">
  </div>
  
  <div class="mb-3">
    <label for="InputPassword" class="form-label">Mot de passe</label>
    <input type="password" class="form-control" name="InputPassword">
  </div>
  <button type="submit" name= "submit" value= "Inscrire" class="btn btn-primary">Submit</button>
</form>



</div>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Accueil</title>
<link href="style/style.css" rel="stylesheet" type="text/css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
</head>
<body>
<div class="container-md">

        <div class="container-sm">
            <img src="img/logo.jpg" class="container-sm" alt="Image du logo Eat & Love">
        </div>
    
          <nav class="navbar navbar-expand-lg bg-success">
            <div class="container-fluid">
              <a class="navbar-brand text-danger manuscript" href="Accueil">Eat & Love</a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                  <li class="nav-item">
                    <a class="nav-link text-white manuscript" href="Carte">Notre Carte</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white manuscript" href="Authentification">Login</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white manuscript" href="Inscription" >Inscription</a>
                  </li>
                  <!-- <li class="nav-item">
                    <a class="nav-link" href="Menu">Mon Menu</a>
                  </li> -->
              </div>
            </div>
          </nav>
     
        
        <div id="carouselExampleCaptions" class="carousel slide justify-content-center" data-bs-ride="false" >
        <div class="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="img/salade-Caprese.jpg" class="d-block w-100" alt="Image d'entrées salade Caprese">
            <div class="carousel-caption d-none d-md-block">
              <h5 class="manuscript text-dark">Des entrées faits avec amour</h5>
            </div>
          </div>
          <div class="carousel-item">
            <img src="img/spaghetti.jpg" class="d-block w-100" alt="Image de plat spaghetti">
            <div class="carousel-caption d-none d-md-block">
              <h5 class="manuscript text-dark">Des plats succulents</h5>
            </div>
          </div>
          <div class="carousel-item">
            <img src="img/tiramisu1.png" class="d-block w-100" alt="Image de dessert tiramisu">
            <div class="carousel-caption d-none d-md-block">
              <h5 class="manuscript text-dark">Des desserts à tomber par terre</h5>
            </div>
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>


        <div class="container-sm d-flex justify-content-center">
          <img src="img/banniere.PNG" alt="Image de banniere logo des réseaux">
        </div>
          

	</div>
	
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
  </body>
</html>
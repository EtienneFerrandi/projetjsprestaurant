<%@page import="model.Article"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Notre Carte</title>
 <title>Site Restaurant</title>
 <link rel="stylesheet" href="style/style.css">
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
</head>
<body>
	<div class="container-md">

        <div class="container-sm">
            <img src="img/logo.jpg" class="container-sm" alt="Image du logo Eat & Love">
        </div>
    
          <nav class="navbar navbar-expand-lg bg-success">
            <div class="container-fluid">
              <a class="navbar-brand text-danger" href="Accueil">Eat & Love</a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                  <li class="nav-item cursive">
                    <a class="nav-link text-white" href="Carte">Notre Carte</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="Authentification">Login</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="Inscription" >Inscription</a>
                  </li>
                  <!-- <li class="nav-item">
                    <a class="nav-link" href="Menu">Mon Menu</a>
                  </li> -->
              </div>
            </div>
          </nav>
          
     	 <h1>Nos Entrées</h1>
          <div class="container text-center">
          	<div class="row">
          	<c:forEach items="${entrees}" var="entree">
          		<div class="col">
          			<div class="card" style="width: 18rem;">
          				<img src="img/${entree.image}" class="card-img-top" alt="Image de ${entree.nomArticle}" width="200" height="200">
          				<div class="card-body">
          					<h5>${entree.nomArticle}</h5>
		 					<p class="card-text">${entree.description}</p>
		 					<span class="badge rounded-pill text-bg-light">${entree.prix}€</span>
		 				</div>
		 			</div>
		 		</div>
		 	</c:forEach>
			</div>
		 </div>
		 
		 
		 <!-- TODO @Nefert : adapter le code ci-dessous pour faire -->
		 <h1>Nos Plats</h1>
          <div class="container text-center">
          	<div class="row">
          	<c:forEach items="${plats}" var="plat">
          		<div class="col">
          			<div class="card" style="width: 18rem;">
          				<img src="img/${plat.image}" class="card-img-top" alt="Image de ${plat.nomArticle}" width="200" height="200">
          				<div class="card-body">
          					<h5>${plat.nomArticle}</h5>
		 					<p class="card-text">${plat.description}</p>
		 					<span class="badge rounded-pill text-bg-light">${plat.prix}€</span>
		 				</div>
		 			</div>
		 		</div>
		 	</c:forEach>
			</div>
		 </div>
		 
		 
		 <h1>Nos Desserts</h1>
          <div class="container text-center">
          	<div class="row">
          	<c:forEach items="${desserts}" var="dessert">
          		<div class="col">
          			<div class="card" style="width: 18rem;">
          				<img src="img/${dessert.image}" class="card-img-top" alt="Image de ${dessert.nomArticle}" width="200" height="200">
          				<div class="card-body">
          					<h5>${dessert.nomArticle}</h5>
		 					<p class="card-text">${dessert.description}</p>
		 					<span class="badge rounded-pill text-bg-light">${dessert.prix}€</span>
		 				</div>
		 			</div>
		 		</div>
		 	</c:forEach>
			</div>
		 </div>
		 
		 <h1>Nos Boissons</h1>
          <div class="container text-center">
          	<div class="row">
          	<c:forEach items="${boissons}" var="boisson">
          		<div class="col">
          			<div class="card" style="width: 18rem;">
          				<img src="img/${boisson.image}" class="card-img-top" alt="Image de ${boisson.nomArticle}" width="200" height="200">
          				<div class="card-body">
          					<h5>${boisson.nomArticle}</h5>
		 					<p class="card-text">${boisson.description}</p>
		 					<span class="badge rounded-pill text-bg-light">${boisson.prix}€</span>
		 				</div>
		 			</div>
		 		</div>
		 	</c:forEach>
			</div>
		 </div>
	<div>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
  </body>
</html>
<%@page import="model.Ligne"%>
<%@page import="model.Panier"%>
<%@page import="model.Ligne"%>
<%@page import="model.Article"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Votre Menu</title>
<link rel="stylesheet" href="style/style.css" type="text/css">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT"
	crossorigin="anonymous">
</head>
<body>

	<div class="container-md">

		<div class="container-sm">
			<img src="img/logo.jpg" class="container-sm"
				alt="Image du logo Eat & Love">
		</div>

		<nav class="navbar navbar-expand-lg bg-success">
		<div class="container-fluid">
			<a class="navbar-brand text-danger" href="Accueil">Eat & Love</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item"><a class="nav-link text-white" href="Carte">Notre Carte</a></li>
					<li class="nav-item"><a class="nav-link text-white" href="Authentification">Login</a></li>
					<li class="nav-item"><a class="nav-link text-white" href="Inscription">Inscription</a></li>
					<li class="nav-item"><a class="nav-link" href="menu.jsp">Mon Menu</a></li>
			</div>
		</div>
		</nav>


		<div class="container-sm">
			<h1 class="text-center">Mon Panier</h1>

			<p class="text-center">
				La commande de
				<%=session.getAttribute("clientConnectPrenom")%>
				<%=session.getAttribute("clientConnectNom")%>
				(Montant panier:
				<%
					Panier panier = (Panier) session.getAttribute("panier");
					out.print(panier.getPrixTotal());
				%>
				€)
			</p> 
			
			<div class="row">
				<div class="col">
					<form action="Menu" method="post">
						<div class="mb-3">
							<%-- <%=request.getAttribute("listeArticles")%> --%>
							<label for="" class="form-label">Fais ton choix parmi nos plats faits maison:</label> 
							<select name="selectArticle" class="form-select" aria-label="Default select example">
							<c:forEach items="${listeArticles}" var="article">
       					  		<option value="${article.idArticle}">${article.nomArticle}</option>
    						</c:forEach>
							</select>
						</div>
						<div class="mb-3">
							<label for="" class="form-label">Quantité</label>
							<input type="number"  name="quantite" class="form-control" min="1">
						</div>
						<button type="submit" name="submit" value="ajouter" class="btn btn-light">Ajouter au panier</button>
					</form>
				</div>

				<div class="col">
					<form action="Recapitulatif" method="post">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Article</th>
									<th>Quantité</th>
									<th>Prix total</th>
								</tr>
							</thead>
							<%
								ArrayList<Ligne> choix = panier.getChoix();	
								String table = "<tbody>";
							
								for(Ligne ligne:choix){
									table += "<tr>";
									table += "<td>" + ligne.getArticle().getNomArticle() + "</td>";
									table += "<td>" + ligne.getQuantite() + "</td>";
									table += "<td>" + ligne.getPrixLigne() + "€</td>";
									table += "</tr>";
								}
								table += "</tbody>";
								out.print(table);
							%>
							<%-- <tbody>
									 		
								<c:forEach var="ligne" items="${panier.choix}">
								<tr>
									<td><c:out value="${ligne.article.nomArticle}" /></td>
									<td><c:out value="${ligne.quantite}" /></td> 
									<td><c:out value="${ligne.prixLigne}" />€</td>
								</tr>
								</c:forEach>
							</tbody> --%>
						</table>
						<div class="text-end">
							<button type="submit" name="submit" value="valider" class="btn btn-success">Valider le panier</button>
						</div>
					</form>


				</div>
			</div>
		</div>
	</div>



	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
		crossorigin="anonymous"></script>
</body>
</html>
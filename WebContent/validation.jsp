<%@page import="model.Panier"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>Validation</title>
 <link rel="stylesheet" href="style/style.css">
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous"> 
</head>
<body>


<div class="container-md">

        <div class="container-sm">
            <img src="img/logo.jpg" class="container-sm" alt="Image du logo Eat & Love">
        </div>
    
          <nav class="navbar navbar-expand-lg bg-success">
            <div class="container-fluid">
              <a class="navbar-brand text-danger" href="Accueil">Eat & Love</a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                  <li class="nav-item">
                    <a class="nav-link text-white" href="Carte">Notre Carte</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="Authentification">Login</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="Inscription" >Inscription</a>
                  </li>
                  <!-- <li class="nav-item">
                    <a class="nav-link" href="Menu">Mon Menu</a>
                  </li> -->
              </div>
            </div>
          </nav>
          
           <div class="container-sm text-center">
            <p class="text-success">Félicitation
            	<%=session.getAttribute("clientConnectPrenom")%>
				<%=session.getAttribute("clientConnectNom")%>
            </p>
          	<p class="text-success">Vore commande est validée.</p>
          	<p class="text-success">Le montant total est de
          		<%
          			Panier panier = (Panier) session.getAttribute("panier");
					Double prixTotal = panier.getPrixTotal();
					out.print(prixTotal);
          		%>
          		€
          	</p>
          	<img src="img/validee.jpg">
        </div>
         
          
          
   </div>

	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
		crossorigin="anonymous"></script>
</body>
</html>
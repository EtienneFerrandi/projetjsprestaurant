package srv;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DaoArticle;
import dao.DaoClient;
import model.Article;
import model.Client;
import model.Ligne;
import model.Panier;

/**
 * Servlet implementation class Menu
 */
@WebServlet("/Menu")
public class Menu extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Menu() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// response.getWriter().append("Served at:
		// ").append(request.getContextPath());

		// Cr�ation d'un objet de session
		HttpSession session = request.getSession();

		// R�cup�ration du panier initialis� et associ� � la session en cours
		Panier panier = (Panier) session.getAttribute("panier");
		Client client = panier.getClient();
		ArrayList<Ligne> lignes = panier.getChoix();
		Double prixTotal = panier.getPrixTotal();

		// Initialisation des autres variables
		DaoArticle d = new DaoArticle();
		ArrayList<Article> listeArticles = null;
		Article articleChoisi = null;

		System.out.println(panier);

		String bouton = request.getParameter("submit");

		try {
			// On r�cup�re la liste des articles de la BD pour cr�er le select
			listeArticles = d.select();
			request.setAttribute("listeArticles", listeArticles);

		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Error1 =====>" + e.getMessage());
		}

		if (bouton.equals("ajouter")) {

			// On r�cup�re les choix du client
			if (!request.getParameter("selectArticle").equals("") && !request.getParameter("quantite").equals("")) {

				int idArticle = Integer.parseInt(request.getParameter("selectArticle"));
				int quantite = Integer.parseInt(request.getParameter("quantite"));

				try {

					// On cr�e les valeurs � afficher pour la s�lection en cours
					articleChoisi = d.selectById(idArticle);
					Double prixLigne = (double) (quantite * articleChoisi.getPrix());

					prixTotal += prixLigne;
					System.out.println("PRIX TOTAL ===> " + prixTotal);

					String nomArticleChoisi = articleChoisi.getNomArticle();
					int indexDoublon = 0;
					Boolean updateDoublon = false;

					for (Ligne ligne : lignes) {
						if (nomArticleChoisi.equals(ligne.getArticle().getNomArticle())) {
							quantite += ligne.getQuantite();
							prixLigne += ligne.getPrixLigne();
							ligne.setQuantite(quantite);
							ligne.setPrixLigne(prixLigne);
							updateDoublon = true;
						}
					}

					if (updateDoublon == false) {
						Ligne ligne = new Ligne(quantite, articleChoisi, prixLigne);
						lignes.add(ligne);
					}

					System.out.println("LIGNES ===> " + lignes);

					panier.setClient(client);
					panier.setChoix(lignes);
					panier.setPrixTotal(prixTotal);
					System.out.println("PANIER ===> " + panier);

					session.setAttribute("panier", panier);
					// request.getRequestDispatcher("menu.jsp").forward(request,
					// response);
					// getServletContext().getRequestDispatcher("/Menu").forward(request,
					// response);

				} catch (Exception e) {
					System.out.println("Error2 =====>" + e.getMessage());
				}

			} else if (bouton.equals("valider")) {

				client = (Client) session.getAttribute("client");
				lignes = (ArrayList<Ligne>) session.getAttribute("lignes");
				prixTotal = (Double) session.getAttribute("prixTotal");

				panier.setClient(client);
				panier.setChoix(lignes);
				panier.setPrixTotal(prixTotal);

				session.setAttribute("panier", panier);

				request.getRequestDispatcher("/Recapitulatif").forward(request, response);

			}

		}

		request.getRequestDispatcher("menu.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package srv;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoArticle;
import model.Article;

/**
 * Servlet implementation class Carte
 */
@WebServlet("/Carte")
public class Carte extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Carte() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		DaoArticle dao = new DaoArticle();
		ArrayList<Article> listeArticles = null;
		 
		ArrayList<Article> entrees = new ArrayList<>();
		ArrayList<Article> plats = new ArrayList<>();
		ArrayList<Article> desserts = new ArrayList<>();
		ArrayList<Article> boissons = new ArrayList<>();
		
		
		try {
			
			listeArticles = dao.select();
			
			for (Article article : listeArticles){
				
				switch (article.getCategorie()){
				case ("entree"):
					entrees.add(article);
					break;
				case ("plat"):
					plats.add(article);
					break;
				case ("dessert"):
					desserts.add(article);
					break;
				case ("boisson"):
					boissons.add(article);
					break;
				default:
					System.out.println("Error : pb catégories des articles");
				}
			}
				
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("ERROR=>" + e.getMessage());
		}
		
		request.setAttribute("desserts",desserts);
		request.setAttribute("plats",plats);
		request.setAttribute("entrees",entrees);
		request.setAttribute("boissons",boissons);
		
		request.getRequestDispatcher("carte.jsp").forward(request,response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

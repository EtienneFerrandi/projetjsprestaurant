package srv;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoArticle;
import dao.DaoClient;
import model.Article;
import model.Client;

/**
 * Servlet implementation class Inscription
 * @param <String>
 */
@WebServlet("/Inscription")
public class Inscription extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Inscription() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter("InputIdentifiant");
		String nom = request.getParameter("InputNom");
        String prenom = request.getParameter("InputPrenom");
        String mail = request.getParameter("InputEmail");
        String tel = request.getParameter("InputTel");
        String adresse = request.getParameter("InputAdress");
        String mdp = request.getParameter("InputPassword");
       
  
        
        
        Client c = new Client (login, nom, prenom, mail, tel, adresse, mdp);
        DaoClient d = new DaoClient();
        
     
        
        if (login !=null){
    			
    	
        try{
        	d.insert(c);
       }
        catch (Exception e){
        	
			System.out.println(e.getMessage());
        }
        request.setAttribute("login", login);
        }
        	
        
        
        request.getRequestDispatcher("inscription.jsp").forward(request,response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
        				
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
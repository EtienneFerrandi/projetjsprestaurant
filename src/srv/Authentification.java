package srv;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DaoClient;
import model.Client;
import model.Ligne;
import model.Panier;

/**
 * Servlet implementation class Authentification
 * @param <Cookie>
 */
@WebServlet("/Authentification")
public class Authentification extends HttpServlet {

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Authentification() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 	
				DaoClient d = new DaoClient();
				Client c = null;
				
				
					String inputLogin = request.getParameter("inputLogin");
					String inputMdp = request.getParameter("inputPassword");
					
					try {
						c = d.selectByIds(inputLogin, inputMdp);
						System.out.println(c);
						
					
				} catch (Exception e) {
					System.out.println("ERROR=====>"+e.getMessage());
				}
				
				if(c==null){
					System.out.println("client inexistant");
					request.setAttribute("clientInexistant",c);
				}
				else{

					
					if(c==null ||inputLogin==null){
						System.out.println("client inexistant");
						request.getRequestDispatcher("authentification.jsp").forward(request,response);
						// todo : éventuellement rediriger sur une page d'erreur
					
					}
					else{
						
						System.out.println("ok");
						HttpSession session = request.getSession();
						
						session.setAttribute("clientConnectNom", c.getNom());
						session.setAttribute("clientConnectPrenom", c.getPrenom());
						
						Double prixTotal = 0.00;
						ArrayList<Ligne> choix = new ArrayList<Ligne>();
						Panier panier = new Panier(c, choix, prixTotal);
						
						session.setAttribute("panier", panier);
						
						System.out.println(session.getAttribute("clientConnectNom"));
						
						getServletContext().getRequestDispatcher("/Menu").forward(request, response);

					}
				}
		
		request.getRequestDispatcher("authentification.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		    
     
		doGet(request, response);
	}

}

package model;

import java.util.ArrayList;

public class Panier {
	private Client client;
	private ArrayList<Ligne> choix;
	private Double prixTotal;
	
	
	
	public Panier(Double prixTotal) {
		this.prixTotal = prixTotal;
	}
	public Double getPrixTotal() {
		return prixTotal;
	}
	public void setPrixTotal(Double prixTotal) {
		this.prixTotal = prixTotal;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public ArrayList<Ligne> getChoix() {
		return choix;
	}
	public void setChoix(ArrayList<Ligne> choix) {
		this.choix = choix;
	}
	
	
	@Override
	public String toString() {
		return "Panier [client=" + client + ", choix=" + choix + ", prixTotal=" + prixTotal + "]";
	}
	
	public Panier(Client client, ArrayList<Ligne> choix, Double prixTotal) {
		this.client = client;
		this.choix = choix;
		this.prixTotal = prixTotal;
	}
	
	public Panier() {
	}
	
}

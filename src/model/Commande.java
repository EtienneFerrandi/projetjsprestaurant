package model;

import java.time.LocalDate;

public class Commande {
	
	private int idCommande;
	private int idClient;
	private LocalDate date;
	private int prix;
	private String infos;
	
	
	public int getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(int idCommande) {
		this.idCommande = idCommande;
	}
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public int getPrix() {
		return prix;
	}
	public void setPrix(int prix) {
		this.prix = prix;
	}
	public String getInfos() {
		return infos;
	}
	public void setInfos(String infos) {
		this.infos = infos;
	}
	
	
	@Override
	public String toString() {
		return "Commande [idCommande=" + null + ", idClient=" + idClient + ", date=" + date + ", prix=" + prix
				+ ", infos=" + infos + "]";
	}
	public Commande(int idCommande, int idClient, LocalDate date, int prix, String infos) {
		this.idCommande = idCommande;
		this.idClient = idClient;
		this.date = date;
		this.prix = prix;
		this.infos = infos;
	}
	
	public Commande() {
	}
	
}

package model;

public class Article {
 private int idArticle;
 private String nomArticle; 
 private String description;
 private int prix;
 private String image;
 private String categorie;
 
 
public Article(int idArticle, String nomArticle, String description, int prix, String image, String categorie) {
	this.idArticle = idArticle;
	this.nomArticle = nomArticle;
	this.description = description;
	this.prix = prix;
	this.image = image;
	this.categorie = categorie;
}
public int getIdArticle() {
	return idArticle;
}
public void setIdArticle(int idArticle) {
	this.idArticle = idArticle;
}
public String getNomArticle() {
	return nomArticle;
}
public void setNomArticle(String nomArticle) {
	this.nomArticle = nomArticle;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public int getPrix() {
	return prix;
}
public void setPrix(int prix) {
	this.prix = prix;
}
public String getImage() {
	return image;
}
public void setImage(String image) {
	this.image = image;
}
public String getCategorie() {
	return categorie;
}
public void setCategorie(String categorie) {
	this.categorie = categorie;
}
@Override
public String toString() {
	return "Article [idArticle=" + idArticle + ", nomArticle=" + nomArticle + ", description=" + description + ", prix="
			+ prix + ", image=" + image + ", categorie=" + categorie + "]";
}
 
public Article(){
	
}
}

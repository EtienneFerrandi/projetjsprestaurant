package model;

public class Ligne {
	private int quantite;
	private Article article;
	private Double prixLigne;
	
	
	public Double getPrixLigne() {
		return prixLigne;
	}
	public void setPrixLigne(Double prixLigne) {
		this.prixLigne = prixLigne;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	
	@Override
	public String toString() {
		return "Ligne [quantite=" + quantite + ", article=" + article + ", prixLigne=" + prixLigne + "]";
	}
	
	public Ligne(int quantite, Article article, Double prixLigne) {
		this.quantite = quantite;
		this.article = article;
		this.prixLigne = prixLigne;
	}

	
	public Ligne() {
	}
	
}

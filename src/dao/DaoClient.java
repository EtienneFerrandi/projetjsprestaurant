package dao;

import java.sql.*;

import model.*;

public class DaoClient {

	public void insert(Client c) throws ClassNotFoundException, SQLException {
		String sql = "insert into client (login, nom, prenom, mail, tel, adresse, mdp) values (\"" + c.getLogin() +"\",\"" + c.getNom() + "\",\""
				+ c.getPrenom() + "\",\"" + c.getMail() + "\",\"" + c.getTel() + "\",\"" + c.getAdresse() + "\",\""
				+ c.getMdp() + "\")";
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db-restaurant", "root", "root");
		System.out.println(sql);
		Statement st = conn.createStatement();
		st.executeUpdate(sql);

		conn.close();
	}

	
	 public Client selectByMail(String mail) throws ClassNotFoundException, SQLException{
         
         Client c = null;
         String sql = "select * from client where mail=\""+mail+"\"";
                 
         Class.forName("com.mysql.jdbc.Driver");
         Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db-restaurant", "root", "root");
         Statement st = conn.createStatement();
         st.executeQuery(sql);
         
         ResultSet rs = st.executeQuery(sql);
           
         while(rs.next()){
        	 c = new Client(rs.getString("login"), rs.getString("nom"), rs.getString("prenom"), rs.getString("adresse"), rs.getString("mail"), rs.getString("tel"),rs.getString("mdp"));
         }
         System.out.println(c);
                 
         conn.close();
         
         return c;
 }
	 
	 public Client selectByLogin(String login) throws ClassNotFoundException, SQLException{
         
         Client c = null;
         String sql = "select * from client where login=\""+login+"\"";
                 
         Class.forName("com.mysql.jdbc.Driver");
         Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db-restaurant", "root", "root");
         Statement st = conn.createStatement();
         st.executeQuery(sql);
         
         ResultSet rs = st.executeQuery(sql);
           
         rs.next();
         c = new Client(rs.getString("login"), rs.getString("nom"), rs.getString("prenom"), rs.getString("adresse"), rs.getString("mail"), rs.getString("tel"),rs.getString("mdp"));
  
         System.out.println(c);
                 
         conn.close();
         
         return c;
	 }
	 
	 public Client selectByIds(String login, String mdp) throws ClassNotFoundException, SQLException{
         
         Client c = null;
         String sql = "select * from client where login=\""+login+"\" and mdp=\""+mdp+"\"";
                 
         Class.forName("com.mysql.jdbc.Driver");
         Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db-restaurant", "root", "root");
         Statement st = conn.createStatement();
         st.executeQuery(sql);
         
         ResultSet rs = st.executeQuery(sql);
           
         rs.next();
         c = new Client(rs.getString("login"), rs.getString("nom"), rs.getString("prenom"), rs.getString("adresse"), rs.getString("mail"), rs.getString("tel"),rs.getString("mdp"));
 
         System.out.println(c);
                 
         conn.close();
         
         return c;
 }
}

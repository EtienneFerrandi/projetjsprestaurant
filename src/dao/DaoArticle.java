package dao;

import java.sql.*;
import java.util.ArrayList;

import model.Article;

public class DaoArticle {

	public ArrayList<Article> select() throws ClassNotFoundException, SQLException {
		ArrayList<Article> liste = new ArrayList<Article>();
		String sql = "select * from article";
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db-restaurant", "root", "root");

		Statement st = conn.createStatement();

		ResultSet rs = st.executeQuery(sql);
		while (rs.next()) {
			Article a = new Article(rs.getInt("idArticle"), rs.getString("nomArticle"), rs.getString("description"),
					rs.getInt("prix"), rs.getString("image"), rs.getString("categorie"));
			liste.add(a);
		}
		conn.close();
		return liste;
	}

	public Article selectById(int idArticle) throws ClassNotFoundException, SQLException {
		Article a = null;
		String sql = "select * from article where idArticle=" + idArticle;
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/db-restaurant", "root", "root");

		Statement st = conn.createStatement();

		ResultSet rs = st.executeQuery(sql);
		while (rs.next())
			a = new Article(rs.getInt("idArticle"), rs.getString("nomArticle"), rs.getString("description"),
					rs.getInt("prix"), rs.getString("image"), rs.getString("categorie"));

		conn.close();
		return a;
	}

}
